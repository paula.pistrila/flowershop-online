<?php
 session_start();

 include("php/config.php");
 if(!isset($_SESSION['valid']))
 {
   header("Location: index.php");
 }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style.css">
    <title>Modifica Profilul</title>
</head>

<body>
    <div class="nav">
        <div class="logo">
            <p><a href="home.php"> Floraria ta online</a></p>
        </div>

        <div class="right-links">
            <a href="#">Change Profile</a>
            <a href="php/logout.php"><button class="btn">Log Out</button></a>
        </div>
    </div>

    <div class="container">
        <div class="box form-box">
            <?php
                if(isset($_POST['submit']))
                {
                    $username=$_POST['username'];
                    $email=$_POST['email'];
                    $telefon=$_POST['telefon'];

                    $id=$_SESSION['id'];

                    $edit_query=mysqli_query($con, "UPDATE users SET Username='$username', Email='$email', Telefon='$telefon' WHERE id=$id") or die("Eroare modificare profil!");
                
                    if($edit_query)
                    {
                        echo "<div class='message'>
                        <p>Modificarea profilului a fost efectuata cu succes!</p>
                        </div>
                        <br>";
                        echo "<a href='home.php'><button class='btn'>Go home</button>";
                        
                    }
                }else{
                    $id=$_SESSION['id'];
                    $querry=mysqli_query($con, "SELECT * FROM users WHERE id=$id");

                    while($result=mysqli_fetch_assoc($querry))
                    {
                        $res_Uname=$result['username'];
                        $res_Email=$result['email'];
                        $res_Telefon=$result['telefon'];
                    }
            ?>
            <header>Modifica Profilul</header>
            <form action="" method="post">
                <div class="field input">
                    <label for="username">Username</label>
                    <input type="text" name="username" value="<?php echo $res_Uname;?>" id="username" required>
                </div>

                <div class="field input">
                    <label for="email">Email</label>
                    <input type="text" name="email" value="<?php echo $res_Email;?>" id="email" required>
                </div>

                <div class="field input">
                    <label for="telefon">Telefon</label>
                    <input type="text" name="telefon" value="<?php echo $res_Telefon;?>" id="telefon" required>
                </div>

                <div class="field">
                    <input type="submit" class="btn" name="submit" value="Update" required>
                </div>

            </form>
        </div>
        <?php } ?>
    </div>

</body>

</html>