<?php
session_start();
require_once('functions/function.php');
include("php/config.php");
if (!isset($_SESSION['valid'])) {
    header("Location: index.php");
}

require_once('component.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="reset.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css" />
    <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
    <!-- Bootstrap CDN -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
-->
    <link rel="stylesheet" href="style/bo.css">
    <link rel="stylesheet" href="style/style.css">

    <title>Home</title>
</head>


<body>

    <div class="nav">
        <div class="logo">
            <p><a href="home.php"> Floraria ta online</a> </p>
        </div>

        <div class="right-links">

            <?php
            $id = $_SESSION['id'];
            $query = mysqli_query($con, "SELECT*FROM users WHERE id=$id");

            while ($result = mysqli_fetch_assoc($query)) {
                $res_Uname = $result['username'];
                $res_Email = $result['email'];
                $res_Cont = $result['cont'];
                $res_Telefon = $result['telefon'];
                $res_Id = $result['id'];
            }

            echo "<a href='edit.php?id=$res_Id'>Change Profile</a>";
            ?>
            <a href="php/logout.php"><button class="btn">Log Out</button></a>


        </div>
    </div>



    <nav class="sidebar close">
        <header>
            <div class="image-text">
                <span class="image">
                    <img src="logo.jpg" alt="logo">
                </span>
                <div class="text header-text">
                    <span class="name">Pagina</span>
                    <span class="profession"><?php echo $res_Cont ?></span>
                </div>
            </div>
            <i class='bx bx-chevron-right toggle'></i>
        </header>
        <div class="menu-bar">
            <div class="menu">
                <ul class="menu-links">

                    <li class="nav-link">
                        <a href="home_client.php">
                            <i class='bx bx-home icon'></i>
                            <span class="text nav-text">HOME</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="buchete.php">
                            <i class='bx bxs-florist icon'></i>
                            <span class="text nav-text">Buchete</span>
                        </a>
                    </li>


                    <li class="nav-link">
                        <a href="cart.php">
                            <i class='bx bx-cart icon'><sup><?php cart_item_number(); ?></sup></i>
                            <span class="text nav-text">Cos de cumparaturi</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="ORDERS.php">
                            <i class='bx bx-shopping-bag icon'></i>
                            <span class="text nav-text">Comenzile mele</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="despreNoi.php">
                            <i class='bx bx-book-open icon'></i>
                            <span class="text nav-text">Despre noi</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="contact.php">
                            <i class='bx bxs-contact icon'></i>
                            <span class="text nav-text">Contact</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </nav>

    <!--- functie adauagare in cos -->
    <?php
    cart();
    ?>

    <script src="script.js"></script>




    <div class="container">
        <div class="row text-center py-5">
            <?php
            $select_query = "Select * from `buchet` order by rand()";
            $result = mysqli_query($con, $select_query);

            while ($row = mysqli_fetch_assoc($result)) {
                $id_produs = $row['id_produs'];
                $nume = $row['nume'];
                $descriere = $row['descriere'];
                $pret = $row['pret'];
                $poza = $row['poza'];

                component($id_produs, $nume, $pret, $descriere, $poza);
            }


            // $ip = getIPAddress();
            // echo 'User Real IP Address - ' . $ip;
            ?>
        </div>

    </div>





</body>

</html>