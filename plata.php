<?php
require_once('functions/function.php');
session_start();

include("php/config.php");

if (isset($_GET['id'])) {
    $id_user = $_GET['id'];
}

$get_ip_add = getIPAddress();
$total_price = 0;
$cart_query = "SELECT * FROM `cart` WHERE ip_address='$get_ip_add'";
$result = mysqli_query($con, $cart_query);
$invoice_number = mt_rand();

$count_products = mysqli_num_rows($result);
while ($row = mysqli_fetch_array($result)) {
    $product_id = $row['id_produs'];
    $select_products = "SELECT * FROM `buchet` WHERE id_produs='$product_id'";
    $result_products = mysqli_query($con, $select_products);

    while ($row_product_price = mysqli_fetch_array($result_products)) {
        $product_price = array($row_product_price['pret']);
        $product_values = array_sum($product_price);
        $total_price += $product_values;
    }
}

//obtinem cantitatea din cos
$get_cart = "select * from `cart`";
$run_cart = mysqli_query($con, $get_cart);
$get_item_quantity = mysqli_fetch_array($run_cart);
$cantitate = $get_item_quantity['cantitate'];

if ($cantitate == 1) {
    $subtotal = $total_price;
} else {
    $cantitate = $cantitate;
    $subtotal = $total_price * $cantitate;
}
if (isset($_POST['submit'])) {
    $cvc = $_POST['cvc'];
    $data_expirare = $_POST['data'];
    ################################## verificaaaaa cum faci display la mesaj
    if ((strlen($cvc) !== 3 || !ctype_digit($cvc)) || strtotime($data_expirare) < strtotime('today')) {
        echo "<script>alert('Data sau CVC gresite! Data trebuie sa fie cel putin ziua de azi, iar CVC-ul trebuie sa contina 3 cifre!!!')</script>";
        header('Location: plata.php');
        exit;
    } else {

        $insert_order = "INSERT INTO `orders`(id_user, cost,nr_comanda,total_produse,data)
values($id_user, $subtotal, $invoice_number, $count_products, NOW())";

        $result_query = mysqli_query($con, $insert_order);

        if ($result_query) {
            $alert_message = "Comanda a fost finalizata cu succes!";
            header('Location: home_client.php?message=' . urlencode($alert_message));
            exit;
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style.css">
    <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
    <title>Home</title>
</head>


<body style="background-image: url('background/5.jpg'); background-size: cover; background-repeat: no-repeat; background-attachment: fixed;">

    <div class="nav">
        <div class="logo">
            <p><a href="home.php"> Floraria ta online</a></p>
        </div>

        <div class="right-links">

            <?php
            $id = $_SESSION['id'];
            $query = mysqli_query($con, "SELECT*FROM users WHERE id=$id");

            while ($result = mysqli_fetch_assoc($query)) {
                $res_Uname = $result['username'];
                $res_Email = $result['email'];
                $res_Cont = $result['cont'];
                $res_Telefon = $result['telefon'];
                $res_Id = $result['id'];
            }

            echo "<a href='edit.php?id=$res_Id'>Change Profile</a>";
            ?>
            <a href="php/logout.php"><button class="btn">Log Out</button></a>
        </div>
    </div>


    <nav class="sidebar close">
        <header>
            <div class="image-text">
                <span class="image">
                    <img src="logo.jpg" alt="logo">
                </span>
                <div class="text header-text">
                    <span class="name">Pagina</span>
                    <span class="profession"><?php echo $res_Cont ?></span>
                </div>
            </div>
            <i class='bx bx-chevron-right toggle'></i>
        </header>
        <div class="menu-bar">
            <div class="menu">
                <ul class="menu-links">

                    <li class="nav-link">
                        <a href="home_client.php">
                            <i class='bx bx-home icon'></i>
                            <span class="text nav-text">HOME</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="buchete.php">
                            <i class='bx bxs-florist icon'></i>
                            <span class="text nav-text">Buchete</span>
                        </a>
                    </li>


                    <li class="nav-link">
                        <a href="cart.php">
                            <i class='bx bx-cart icon'><sup><?php cart_item_number(); ?></sup></i>
                            <span class="text nav-text">Cos de cumparaturi</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="despreNoi.php">
                            <i class='bx bx-book-open icon'></i>
                            <span class="text nav-text">Despre noi</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="contact.php">
                            <i class='bx bxs-contact icon'></i>
                            <span class="text nav-text">Contact</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </nav>
    <script src="script.js"></script>


    <div class="container">
        <div class="box form-box">
            <header>Detalii Card</header>
            <form action="" method="post">

                <div class="field input">
                    <label for="judet">Județ</label>
                    <select name="judet" id="judet">
                        <?php
                        $judete = array(
                            "Alba", "Arad", "Argeș", "Bacău", "Bihor", "Bistrița-Năsăud", "Botoșani", "Brăila",
                            "Brașov", "Buzău", "Călărași", "Caraș-Severin", "Cluj", "Constanța", "Covasna",
                            "Dâmbovița", "Dolj", "Galați", "Giurgiu", "Gorj", "Harghita", "Hunedoara", "Ialomița",
                            "Iași", "Ilfov", "Maramureș", "Mehedinți", "Mureș", "Neamț", "Olt", "Prahova", "Satu Mare",
                            "Sălaj", "Sibiu", "Suceava", "Teleorman", "Timiș", "Tulcea", "Vâlcea", "Vaslui", "Vrancea"
                        );

                        foreach ($judete as $judet) {
                            echo "<option value='$judet'>$judet</option>";
                        }
                        ?>
                    </select>
                </div>

                <div class="field input">
                    <label for="oras">Oras</label>
                    <input type="text" name="oras" id="oras" required>
                </div>

                <div class="field input">
                    <label for="adresa">Strada si Nr</label>
                    <input type="text" name="adresa" id="adresa" required>
                </div>

                <div class="field input">
                    <label for="card">Numar Card</label>
                    <input type="text" name="card" id="card" required>
                </div>

                <div class="field input">
                    <label for="data">Data expirare</label>
                    <input type="date" name="data" id="data" required>
                </div>

                <div class="field input">
                    <label for="cvc">CVC</label>
                    <input type="text" name="cvc" id="cvc" required>
                </div>

                <div class="field input">
                    <p>Total Plata: <?php echo "$total_price" ?>$</p>

                </div>


                <div class="field">
                    <input type="submit" class="btn" name="submit" value="Trimite Comanda" required>
                </div>

            </form>

        </div>
    </div>


</body>

</html>