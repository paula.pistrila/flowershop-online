// Array cu URL-uri pentru imagini de fundal
var imaginiDeFundal = [
    'background/1.jpg',
    'background/2.jpg',
    'background/3.jpg',
    'background/4.jpg',
    'background/5.jpg',
    'background/6.jpg'

];

var indexCurent = 0; // Indexul imaginii curente

function schimbaImagineDeFundal() {
    document.querySelector('.background-container').style.backgroundImage = 'url(' + imaginiDeFundal[indexCurent] + ')';
    indexCurent = (indexCurent + 1) % imaginiDeFundal.length;
}


// Setarea unui interval pentru a schimba imaginile la fiecare 5 secunde
setInterval(schimbaImagineDeFundal, 5000); // 5000 milisecunde = 5 secunde

const body = document.querySelector("body"),
    sidebar = body.querySelector(".sidebar"),
    toggle = body.querySelector(".toggle");



toggle.addEventListener("click", () => {
    sidebar.classList.toggle("close");
});


