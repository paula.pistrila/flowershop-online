<?php
require_once('functions/function.php');
session_start();

include("php/config.php");
if (!isset($_SESSION['valid'])) {
    header("Location: index.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style.css">
    <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
    <title>Despre Noi</title>

    <style>
        .dynamic-list {
            width: 70%;
            list-style-type: none;
            padding: 20px;
            margin: 0 auto;
            align-items: center;
        }

        .dynamic-list li {
            position: relative;
            padding: 20px;
            background-color: #695CFE;
            color: #fff;
            border: 2px solid #8e44ad;
            border-radius: 10px;
            margin-bottom: 10px;
        }

        .arrow {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            cursor: pointer;
            font-size: 24px;
            color: #695CFE;
        }

        .next {
            right: -25px;
        }

        .prev {
            left: -25px;
        }

        .descriere {
            padding: 20px;
            color: purple;
            font-weight: 600;
            background-color: #d9d2e9;
            max-width: 90%;
            border-radius: 10px;
            margin: 0 auto;


        }

        .despre_noi {
            display: flex;
            justify-content: center;
            align-items: center;
            background-image: url('background/4.jpg');
            background-repeat: no-repeat;
            background-size: cover;
            height: 100vh;
            padding: 20px;
            margin-left: 56px;
        }
    </style>
</head>


<body>
    <div class="nav">
        <div class="logo">
            <p><a href="home.php"> Floraria ta online</a></p>
        </div>

        <div class="right-links">

            <?php
            $id = $_SESSION['id'];
            $query = mysqli_query($con, "SELECT*FROM users WHERE id=$id");

            while ($result = mysqli_fetch_assoc($query)) {
                $res_Uname = $result['username'];
                $res_Email = $result['email'];
                $res_Cont = $result['cont'];
                $res_Telefon = $result['telefon'];
                $res_Id = $result['id'];
            }

            echo "<a href='edit.php?id=$res_Id'>Change Profile</a>";
            ?>
            <a href="php/logout.php"><button class="btn">Log Out</button></a>
        </div>
    </div>



    <nav class="sidebar close">
        <header>
            <div class="image-text">
                <span class="image">
                    <img src="logo.jpg" alt="logo">
                </span>
                <div class="text header-text">
                    <span class="name">Pagina</span>
                    <span class="profession"><?php echo $res_Cont ?></span>
                </div>
            </div>
            <i class='bx bx-chevron-right toggle'></i>
        </header>
        <div class="menu-bar">
            <div class="menu">
                <ul class="menu-links">

                    <li class="nav-link">
                        <a href="home_client.php">
                            <i class='bx bx-home icon'></i>
                            <span class="text nav-text">HOME</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="buchete.php">
                            <i class='bx bxs-florist icon'></i>
                            <span class="text nav-text">Buchete</span>
                        </a>
                    </li>


                    <li class="nav-link">
                        <a href="cart.php">
                            <i class='bx bx-cart icon'><sup><?php cart_item_number(); ?></sup></i>
                            <span class="text nav-text">Cos de cumparaturi</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="ORDERS.php">
                            <i class='bx bx-shopping-bag icon'></i>
                            <span class="text nav-text">Comenzile mele</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="despreNoi.php">
                            <i class='bx bx-book-open icon'></i>
                            <span class="text nav-text">Despre noi</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="contact.php">
                            <i class='bx bxs-contact icon'></i>
                            <span class="text nav-text">Contact</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </nav>

    <script src="script.js"></script>
    <div class="container">
        <div class="despre_noi">
            <div class="c1">
                <div class="descriere">
                    Bine ai venit la <b>Floraria Ta Online</b> - grădina noastră virtuală de frumusețe florală! Suntem mândri să
                    împărtășim cu tine pasiunea noastră pentru flori și aranjamente unice, care aduc bucurie și culoare în viața ta.
                    Iată de ce suntem speciali:
                </div>
                <ul class="dynamic-list">
                    <li>
                        <div class="arrow prev">&#9664;</div>
                        1. Varietate bogată de flori: Ne dedicăm să oferim o gamă vastă de flori proaspete și frumoase. De la
                        trandafiri romantici la bujori parfumați sau crini eleganți, avem cea mai selectă colecție de flori pentru
                        fiecare ocazie.
                        <div class="arrow next">&#9654;</div>
                    </li>
                    <li>
                        <div class="arrow prev">&#9664;</div>
                        2. Aranjamente personalizate: Ne străduim să creăm aranjamente florale personalizate pentru a potența momentele
                        speciale din viața ta. Indiferent dacă este vorba de o zi de naștere, o aniversare sau pur și simplu vrei
                        să-ți impresionezi persoana dragă, avem opțiuni pentru toate gusturile.
                        <div class="arrow next">&#9654;</div>
                    </li>
                    <li>
                        <div class="arrow prev">&#9664;</div>
                        3. Livare rapidă și sigură: Comandă cu încredere de la noi, știind că flori tale vor fi livrate în siguranță la
                        destinație, în timp util. Oferim servicii de livrare rapidă și profesionale pentru a-ți aduce bucuria direct
                        la ușă.
                        <div class="arrow next">&#9654;</div>
                    </li>
                    <li>
                        <div class="arrow prev">&#9664;</div>
                        4. Calitate superioară: Ne străduim să oferim doar cele mai proaspete flori și cele mai frumoase aranjamente.
                        Fiecare floare este selecționată cu grijă și atenție pentru a ne asigura că clienții noștri primesc cea mai
                        bună calitate.
                        <div class="arrow next">&#9654;</div>
                    </li>
                    <li>
                        <div class="arrow prev">&#9664;</div>
                        5. Echipă dedicată: Suntem o echipă pasionată de flori și servicii excelente. Ne place să oferim sfaturi și
                        consultanță pentru a vă ajuta să alegeți cele mai potrivite flori și aranjamente pentru evenimentul sau
                        ocazia dvs. specială.
                        <div class="arrow next">&#9654;</div>
                    </li>
                    <li>
                        <div class="arrow prev">&#9664;</div>
                        6. Experiență online ușoară: Facem cumpărăturile de flori ușoare și convenabile. Site-ul nostru web este
                        prietenos, cu opțiuni simple de căutare și comandă pentru a face achizițiile online o plăcere.
                        <div class="arrow next">&#9654;</div>
                    </li>
                    <li>
                        <div class="arrow prev">&#9664;</div>
                        7. Pasiune pentru mediu: Suntem conștienți de impactul nostru asupra mediului și ne străduim să folosim
                        practici sustenabile. Lucrăm pentru a minimiza deșeurile și a susține mediu în care trăim.
                        <div class="arrow next">&#9654;</div>
                    </li>
                </ul>
                <div class="descriere">
                    La <b>Floraria Ta Online</b>, credem că florile nu sunt doar plante, ci expresia emoțiilor și a iubirii. Alegând
                    florile noastre, alegeți bucuria, frumusețea și calitatea. Cu noi, fiecare floare este o poveste și fiecare
                    aranjament este o capodoperă florală. Te invităm să descoperi minunăția lumii florilor cu noi și să transformi
                    orice moment într-unul special.
                    <br><br>
                    <b><i>Mulțumim că ne-ai ales pentru a face parte din cele mai frumoase momente din viața ta!
                        </i></b>
                </div>
            </div>
        </div>
    </div>
    <script>
        // JavaScript pentru a face lista dinamică navigabilă
        const listItems = document.querySelectorAll('.dynamic-list li');
        let currentIndex = 0;

        // Funcția pentru afișarea elementului la indexul specificat
        function showElement(index) {
            listItems.forEach((item, i) => {
                if (i === index) {
                    item.style.display = 'block';
                } else {
                    item.style.display = 'none';
                }
            });
        }

        // Afiseaza primul element la incarcarea paginii
        showElement(currentIndex);

        // Adaugă un ascultător de eveniment pentru săgețile "Next" și "Previous"
        document.querySelectorAll('.next').forEach((next, i) => {
            next.addEventListener('click', () => {
                currentIndex = (currentIndex + 1) % listItems.length;
                showElement(currentIndex);
            });
        });

        document.querySelectorAll('.prev').forEach((prev, i) => {
            prev.addEventListener('click', () => {
                currentIndex = (currentIndex - 1 + listItems.length) % listItems.length;
                showElement(currentIndex);
            });
        });
    </script>

</body>

</html>