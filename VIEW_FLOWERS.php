<?php
session_start();
require_once('functions/function.php');
include("php/config.php");
if (!isset($_SESSION['valid'])) {
    header("Location: index.php");
}

if (isset($_GET['EDIT_PRODUCTS.php'])) {
    include('EDIT_PRODUCTS.php');
}

if (isset($_GET['delete_product.php'])) {
    include('delete_product.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/bo.css">
    <link rel="stylesheet" href="style/style.css">
    <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css">

    <style>
        .cart_img {
            width: 80px;
            height: 80px;
            object-fit: contain;
        }

        .table {
            background-color: #d9d2e9;
        }
    </style>
    <title>Home</title>
</head>


<body style="background-image: url('background/5.jpg'); background-size: cover; background-repeat: no-repeat; background-attachment: fixed;">

    <div class="nav">
        <div class="logo">
            <p><a href="home.php"> Floraria ta online</a></p>
        </div>

        <div class="right-links">

            <?php
            $id = $_SESSION['id'];
            $query = mysqli_query($con, "SELECT*FROM users WHERE id=$id");

            while ($result = mysqli_fetch_assoc($query)) {
                $res_Uname = $result['username'];
                $res_Email = $result['email'];
                $res_Cont = $result['cont'];
                $res_Telefon = $result['telefon'];
                $res_Id = $result['id'];
            }

            echo "<a href='edit.php?id=$res_Id'>Change Profile</a>";
            ?>
            <a href="php/logout.php"><button class="btn">Log Out</button></a>
        </div>
    </div>


    <nav class="sidebar close">
        <header>
            <div class="image-text">
                <span class="image">
                    <img src="logo.jpg" alt="logo">
                </span>
                <div class="text header-text">
                    <span class="name">Pagina</span>
                    <span class="profession"><?php echo $res_Cont ?></span>
                </div>
            </div>
            <i class='bx bx-chevron-right toggle'></i>
        </header>
        <div class="menu-bar">
            <div class="menu">
                <ul class="menu-links">

                    <li class="nav-link">
                        <a href="home.php">
                            <i class='bx bx-home icon'></i>
                            <span class="text nav-text">HOME</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="VIEW_FLOWERS.php">
                            <i class='bx bxs-florist icon'></i>
                            <span class="text nav-text">Vizualizare Buchete</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="ADD.php">
                            <i class='bx bx-plus icon'></i>
                            <span class="text nav-text">Adauga Buchet</span>
                        </a>
                    </li>


                    <li class="nav-link">
                        <a href="ORDERS_ADMIN.php">
                            <i class='bx bx-shopping-bag icon'></i>
                            <span class="text nav-text">Comenzi</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="VIEW_USER.php">
                            <i class='bx bx-user icon'></i>
                            <span class="text nav-text">Utilizatori</span>
                        </a>
                    </li>

                </ul>
            </div>

        </div>
    </nav>
    <script src="script.js"></script>


    <div class="container">

        <div class="row">
            <form action="" method="post">

                <table class="table table-bordered text-center">
                    <thead>
                        <tr>
                            <th>Nr.</th>
                            <th>Nume Produs</th>
                            <th>Imagine Produs</th>
                            <th>Pret</th>
                            <th>Total produse vandute</th>
                            <th>Editare</th>
                            <th>Sterge</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $get_products = "SELECT * FROM `buchet`";
                        $result = mysqli_query($con, $get_products);
                        $nr = 0;
                        while ($row = mysqli_fetch_assoc($result)) {
                            $id_produs = $row['id_produs'];
                            $nume = $row['nume'];
                            $pret = $row['pret'];
                            $poza = $row['poza'];
                            // $total = $row['total'];
                            $nr++;
                        ?>

                            <tr>
                                <td><?php echo $nr ?></td>
                                <td><?php echo  $nume ?></td>
                                <td><img class='cart_img' src='buchete/<?php echo $poza ?>'></td>
                                <td><?php echo $pret ?>$</td>
                                <td>
                                    <?php
                                    $get_count = "SELECT * FROM `orders` WHERE id=$id_produs";
                                    $result_count = mysqli_query($con, $get_count);

                                    if ($result_count) {
                                        $row = mysqli_fetch_assoc($result_count);
                                        if ($row !== null) {
                                            $total_produse = $row['total_produse'];
                                            echo $total_produse;
                                        } else {
                                            echo "0";
                                        }
                                    }
                                    ?>
                                </td>
                                <td><a href='EDIT_PRODUCTS.php?id=<?php echo $id_produs ?>'><i style='color:grey' class='fa-solid fa-pen-to-square icon'></a></td>
                                <td><a href='delete_product.php?delete_product=<?php echo $id_produs ?>'><i style='color:grey' class='fa-solid fa-trash icon'></a></td>

                            </tr>

                        <?php
                        }
                        ?>

                    </tbody>
                </table>
            </form>
        </div>
    </div>





</body>

</html>