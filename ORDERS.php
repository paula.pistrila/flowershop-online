<?php
require_once('functions/function.php');
session_start();

include("php/config.php");
if (!isset($_SESSION['valid'])) {
    header("Location: index.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/bo.css">
    <link rel="stylesheet" href="style/style.css">
    <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
    <style>
        .table {
            background-color: #d9d2e9;

        }
    </style>
    <title>Home</title>
</head>


<body style="background-image: url('background/5.jpg'); background-size: cover; background-repeat: no-repeat; background-attachment: fixed;">

    <div class="nav">
        <div class="logo">
            <p><a href="home.php"> Floraria ta online</a></p>
        </div>

        <div class="right-links">

            <?php
            $id = $_SESSION['id'];
            $query = mysqli_query($con, "SELECT*FROM users WHERE id=$id");

            while ($result = mysqli_fetch_assoc($query)) {
                $res_Uname = $result['username'];
                $res_Email = $result['email'];
                $res_Cont = $result['cont'];
                $res_Telefon = $result['telefon'];
                $res_Id = $result['id'];
            }

            echo "<a href='edit.php?id=$res_Id'>Change Profile</a>";
            ?>
            <a href="php/logout.php"><button class="btn">Log Out</button></a>
        </div>
    </div>



    <nav class="sidebar close">
        <header>
            <div class="image-text">
                <span class="image">
                    <img src="logo.jpg" alt="logo">
                </span>
                <div class="text header-text">
                    <span class="name">Pagina</span>
                    <span class="profession"><?php echo $res_Cont ?></span>
                </div>
            </div>
            <i class='bx bx-chevron-right toggle'></i>
        </header>
        <div class="menu-bar">
            <div class="menu">
                <ul class="menu-links">

                    <li class="nav-link">
                        <a href="home_client.php">
                            <i class='bx bx-home icon'></i>
                            <span class="text nav-text">HOME</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="buchete.php">
                            <i class='bx bxs-florist icon'></i>
                            <span class="text nav-text">Buchete</span>
                        </a>
                    </li>


                    <li class="nav-link">
                        <a href="cart.php">
                            <i class='bx bx-cart icon'><sup><?php cart_item_number(); ?></sup></i>
                            <span class="text nav-text">Cos de cumparaturi</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="ORDERS.php">
                            <i class='bx bx-shopping-bag icon'></i>
                            <span class="text nav-text">Comenzile mele</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="despreNoi.php">
                            <i class='bx bx-book-open icon'></i>
                            <span class="text nav-text">Despre noi</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="contact.php">
                            <i class='bx bxs-contact icon'></i>
                            <span class="text nav-text">Contact</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </nav>
    <script src="script.js"></script>


    <div class="container">
        <?php
        $username = $_SESSION['username'];
        $get_user = "SELECT * FROM `users` WHERE username='$username'";
        $result = mysqli_query($con, $get_user);
        $row_fetch = mysqli_fetch_assoc($result);
        $user_id = $row_fetch['id'];


        ?>
        <table class="table table-bordered">
            <thead style="background-color:#b4a7d6">
                <tr>
                    <th>Nr.</th>
                    <th>Id Comanda</th>
                    <th>Suma</th>
                    <th>Nr. Produse</th>
                    <th>Nr. Comanda</th>
                    <th>Data</th>

                </tr>
            </thead>
            <tbody>
                <?php
                $get_order_details = "SELECT * FROM `orders` WHERE id_user=$user_id";
                $result_orders = mysqli_query($con, $get_order_details);
                $nr = 1;
                while ($row_orders = mysqli_fetch_assoc($result_orders)) {
                    $order_id = $row_orders['id'];
                    $cost = $row_orders['cost'];
                    $nr_comanda = $row_orders['nr_comanda'];
                    $total_produse = $row_orders['total_produse'];
                    $data = $row_orders['data'];

                    echo "
                     <tr>
                    <td>$nr</td>
                    <td>$order_id</td>
                    <td>$cost</td>
                    <td>$total_produse</td>
                    <td>$nr_comanda</td>
                    <td>$data</td>
                </tr>
                    ";
                    $nr++;
                }
                ?>

            </tbody>
        </table>

    </div>

</body>

</html> ;