<?php
session_start();

include("php/config.php");
if (!isset($_SESSION['valid'])) {
    header("Location: index.php");
}
if (isset($_GET['id'])) {
    $edit_id = $_GET['id'];
    $get_data = "SELECT * FROM `buchet` WHERE id_produs=$edit_id";
    $result = mysqli_query($con, $get_data);
    $row = mysqli_fetch_assoc($result);
    $nume = $row['nume'];
    $pret = $row['pret'];
    $poza = $row['poza'];
    $descriere = $row['descriere'];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style.css">
    <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
    <style>
        .cart_img {
            width: 90px;
            height: 90px;
            margin-left: 30px;
            object-fit: contain;
        }
    </style>
    <title>Home</title>
</head>


<body style="background-image: url('background/5.jpg'); background-size: cover; background-repeat: no-repeat; background-attachment: fixed;">

    <div class="nav">
        <div class="logo">
            <p><a href="home.php"> Floraria ta online</a></p>
        </div>

        <div class="right-links">

            <?php
            $id = $_SESSION['id'];
            $query = mysqli_query($con, "SELECT*FROM users WHERE id=$id");

            while ($result = mysqli_fetch_assoc($query)) {
                $res_Uname = $result['username'];
                $res_Email = $result['email'];
                $res_Cont = $result['cont'];
                $res_Telefon = $result['telefon'];
                $res_Id = $result['id'];
            }

            echo "<a href='edit.php?id=$res_Id'>Change Profile</a>";
            ?>
            <a href="php/logout.php"><button class="btn">Log Out</button></a>
        </div>
    </div>


    <nav class="sidebar close">
        <header>
            <div class="image-text">
                <span class="image">
                    <img src="logo.jpg" alt="logo">
                </span>
                <div class="text header-text">
                    <span class="name">Pagina</span>
                    <span class="profession"><?php echo $res_Cont ?></span>
                </div>
            </div>
            <i class='bx bx-chevron-right toggle'></i>
        </header>
        <div class="menu-bar">
            <div class="menu">
                <ul class="menu-links">

                    <li class="nav-link">
                        <a href="home.php">
                            <i class='bx bx-home icon'></i>
                            <span class="text nav-text">HOME</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="VIEW_FLOWERS.php">
                            <i class='bx bxs-florist icon'></i>
                            <span class="text nav-text">Vizualizare Buchete</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="ADD.php">
                            <i class='bx bx-plus icon'></i>
                            <span class="text nav-text">Adauga Buchet</span>
                        </a>
                    </li>


                    <li class="nav-link">
                        <a href="ORDERS_ADMIN.php">
                            <i class='bx bx-shopping-bag icon'></i>
                            <span class="text nav-text">Comenzi</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="VIEW_USER.php">
                            <i class='bx bx-user icon'></i>
                            <span class="text nav-text">Utilizatori</span>
                        </a>
                    </li>

                </ul>
            </div>

        </div>
    </nav>



    <div class="container">
        <div class="box form-box">
            <h2>Editeaza Produs</h2>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="field input">
                    <label for="nume">Nume</label>
                    <input type="text" name="nume" id="nume" value="<?php echo $nume; ?>" autocomplete="off" required="required">
                </div>

                <div class="field input">
                    <label for="pret">Pret</label>
                    <input type="text" name="pret" id="pret" value="<?php echo $pret; ?>" autocomplete="off" required="required">
                </div>

                <div class="field input">
                    <label for="descriere">Descriere</label>
                    <input type="text" name="descriere" id="descreiere" value="<?php echo $descriere; ?>" autocomplete="off" required>
                </div>

                <div class="field input">
                    <label for="poza">Poza</label>
                    <div style="display: flex; align-items: center;">
                        <input type="file" name="poza" id="poza" required="required">
                        <img src="buchete/.\<?php echo $poza; ?>" alt="" class="cart_img">
                    </div>
                </div>


                <div class="field">
                    <input type="submit" class="btn" name="edit" value="Salveaza Modificarile" required>
                </div>
            </form>
        </div>



    </div>
    <!--editare produs-->
    <?php
    if (isset($_POST['edit'])) {
        $nume = $_POST['nume'];
        $pret = $_POST['pret'];
        $descriere = $_POST['descriere'];

        $poza = $_FILES['poza']['name'];
        $temp_poza = $_FILES['poza']['tmp_name'];
        move_uploaded_file($temp_poza, "buchete/$poza");
        $update_product = "UPDATE `buchet` SET nume=?, pret=?, descriere=?, poza=?, data=NOW() WHERE id_produs=?";
        $stmt = mysqli_prepare($con, $update_product);
        if ($stmt) {
            mysqli_stmt_bind_param($stmt, "ssssi", $nume, $pret, $descriere, $poza, $edit_id);
            if (mysqli_stmt_execute($stmt)) {
                echo "<script>alert('Modificarile au fost efectuate cu succes')</script>";
                echo "<script>window.open('VIEW_FLOWERS.php', '_self')</script>";
            } else {
                echo "Eroare: " . mysqli_error($con);
            }
            mysqli_stmt_close($stmt);
        } else {
            echo "Eroare: " . mysqli_error($con);
        }
    }
    ?>


</body>

</html>