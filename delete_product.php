<?php
session_start();
require_once('functions/function.php');
include("php/config.php");

if (!isset($_SESSION['valid'])) {
    header("Location: index.php");
}

if (isset($_GET['delete_product'])) {
    $delete_id = $_GET['delete_product'];
    echo $delete_id;
    $delete_product = "DELETE FROM `buchet` WHERE id_produs=$delete_id";
    $result = mysqli_query($con, $delete_product);

    if ($result) {
        echo "<script>alert('Produsul a fost sters cu succes')</script>";
        echo "<script>window.open('VIEW_FLOWERS.php', '_self')</script>";
    }
}
