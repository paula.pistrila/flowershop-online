<?php

function component($id_produs, $nume, $pret, $descriere, $poza)
{
    $element = "
    <div class=\"col-md-3 col-sm-6 my-3 my-md-0\">
                <form action=\"buchete.php\" method=\"post\">
                    <div class=\"card shadow\">
                        <div>
                            <img src=\"./buchete/$poza\" alt=\"buchet\" class=\"img-fluid card-img-top\">
                        </div>
                        <div class=\"card-body\">
                            <h5 class=\"card-title\" style='height:100px'>$nume</h5>
                            <h6>
                                <i class=\"fas fa-star\"></i>
                                <i class=\"fas fa-star\"></i>
                                <i class=\"fas fa-star\"></i>
                                <i class=\"fas fa-star\"></i>
                                <i class=\"fas fa-star\"></i>
                            </h6>
                            <p class=\"card-text\" style='height:150px'>$descriere</p>
                            <h5>
                            
                                <span class=\"price\">$pret$</span>
                            </h5>
                            <a href='buchete.php?add_to_cart=$id_produs' type=\"submit\" class=\"btn  my-3\" name=\"add\">Adauga in Cos<i class=\"fas fa-shopping-cart\"></i></a>
                        </div>
                    </div>
                </form>
            </div>
    ";



    echo $element;
}
