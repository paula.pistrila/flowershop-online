<?php require_once('functions/function.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style.css">
    <title>Register</title>
</head>

<body>
    <div class="container">
        <div class="box form-box">

            <?php

            include("php/config.php");
            if (isset($_POST['submit'])) {
                $username = $_POST['username'];
                $cont = $_POST['cont'];
                $email = $_POST['email'];
                $password = $_POST['password'];
                $telefon = $_POST['telefon'];

                //verificam sa avem email unic

                $verify_query = mysqli_query($con, "SELECT Email FROM users WHERE Email='$email'");

                if (mysqli_num_rows($verify_query) != 0) {
                    echo "<div class='message'>
                      <p>Acest email e folosit, Incearca altul!</p>
                  </div> <br>";
                    echo "<a href='javascript:self.history.back()'><button class='btn'>Go Back</button>";
                } else {

                    mysqli_query($con, "INSERT INTO users(Username,Email,cont,Password,telefon) VALUES('$username','$email','$cont','$password', '$telefon')") or die("Eroare inregistrare");

                    echo "<div class='message'>
                      <p>Inregistrat cu succes!</p>
                  </div> <br>";
                    echo "<a href='index.php'><button class='btn'>Login Now</button>";
                }
            } else {

            ?>

                <header>Sign Up</header>
                <form action="" method="post">
                    <div class="field input">
                        <label for="username">Username</label>
                        <input type="text" name="username" id="username" required>
                    </div>

                    <div class="field input">
                        <label for="email">Email</label>
                        <input type="text" name="email" id="email" required>
                    </div>

                    <div class="field input">
                        <label for="telefon">Telefon</label>
                        <input type="text" name="telefon" id="telefon" required>
                    </div>

                    <div class="field input">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password" required>
                    </div>
                    <div class="form-group">
                        <label for="cont">Functie</label>
                        <div>
                            <label for="client" class="radio-inline"><input type="radio" name="cont" value="client" id="client">Client</label>
                            <label for="admin" class="radio-inline"><input type="radio" name="cont" value="admin" id="admin">Admin</label>
                        </div>

                        <div class="field">
                            <input type="submit" class="btn" name="submit" value="Register" required>
                        </div>

                        <div class="links">
                            Ai deja un cont? <a href="index.php">Sing In</a>
                        </div>

                </form>
        </div>
    <?php }


    ?>


    </div>
</body>

</html>