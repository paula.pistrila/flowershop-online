<?php
require_once('functions/function.php');
session_start();

include("php/config.php");
if (!isset($_SESSION['valid'])) {
    header("Location: index.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/bo.css">
    <link rel="stylesheet" href="style/style.css">
    <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>
    <style>
        .cart_img {
            width: 80px;
            height: 80px;
            object-fit: contain;
        }
    </style>
    <title>Home</title>
</head>


<body style="background-color:whitesmoke">
    <div class="nav">
        <div class="logo">
            <p><a href="home.php"> Floraria ta online</a></p>
        </div>

        <div class="right-links">

            <?php
            $id = $_SESSION['id'];
            $query = mysqli_query($con, "SELECT*FROM users WHERE id=$id");

            while ($result = mysqli_fetch_assoc($query)) {
                $res_Uname = $result['username'];
                $res_Email = $result['email'];
                $res_Cont = $result['cont'];
                $res_Telefon = $result['telefon'];
                $res_Id = $result['id'];
            }

            echo "<a href='edit.php?id=$res_Id'>Change Profile</a>";
            ?>
            <a href="php/logout.php"><button class="btn">Log Out</button></a>
        </div>
    </div>



    <nav class="sidebar close">
        <header>
            <div class="image-text">
                <span class="image">
                    <img src="logo.jpg" alt="logo">
                </span>
                <div class="text header-text">
                    <span class="name">Pagina</span>
                    <span class="profession"><?php echo $res_Cont ?></span>
                </div>
            </div>
            <i class='bx bx-chevron-right toggle'></i>
        </header>
        <div class="menu-bar">
            <div class="menu">
                <ul class="menu-links">

                    <li class="nav-link">
                        <a href="home_client.php">
                            <i class='bx bx-home icon'></i>
                            <span class="text nav-text">HOME</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="buchete.php">
                            <i class='bx bxs-florist icon'></i>
                            <span class="text nav-text">Buchete</span>
                        </a>
                    </li>


                    <li class="nav-link">
                        <a href="cart.php">
                            <i class='bx bx-cart icon'><sup><?php cart_item_number(); ?></sup></i>
                            <span class="text nav-text">Cos de cumparaturi</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="ORDERS.php">
                            <i class='bx bx-shopping-bag icon'></i>
                            <span class="text nav-text">Comenzile mele</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="despreNoi.php">
                            <i class='bx bx-book-open icon'></i>
                            <span class="text nav-text">Despre noi</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="contact.php">
                            <i class='bx bxs-contact icon'></i>
                            <span class="text nav-text">Contact</span>
                        </a>
                    </li>
                </ul>
            </div>

        </div>
    </nav>
    <script src="script.js"></script>


    <div class="container">
        <div class="row">
            <form action="" method="post">

                <table class="table table-bordered text-center">

                    <?php

                    //calculez pret total
                    $get_ip_add = getIPAddress();
                    $total_price = 0;
                    $cart_query = "SELECT * FROM `cart` WHERE ip_address='$get_ip_add'";
                    $result = mysqli_query($con, $cart_query);
                    $result_count = mysqli_num_rows($result);
                    if ($result_count > 0) {
                        echo "
                            <thead>
                            <tr>
                                <th>Nume Produs</th>
                                <th>Imagine Produs</th>
                                <th>Catitate</th>
                                <th>Pret Total</th>
                                <th>Sterge Produs</th>
                                <th colspan='2'>Operatii</th>
                            </tr>
                        </thead>
                        <tbody>
                            ";
                        while ($row = mysqli_fetch_array($result)) {
                            $product_id = $row['id_produs'];
                            $cantitate = $row['cantitate'];
                            $select_products = "SELECT * FROM `buchet` WHERE id_produs='$product_id'";
                            $result_products = mysqli_query($con, $select_products);

                            while ($row_product_price = mysqli_fetch_array($result_products)) {
                                $pret = array($row_product_price['pret']);
                                $price_table = $row_product_price['pret'];
                                $nume = $row_product_price['nume'];
                                $poza = $row_product_price['poza'];
                                $product_values = array_sum($pret);
                                $total_price += $product_values;

                    ?>
                                <tr>
                                    <td><?php echo $nume ?></td>
                                    <td><img src="./buchete/<?php echo $poza ?>" alt="" class="cart_img"></td>
                                    <td><input type="text" name="cantitate" value="<?php echo $cantitate; ?>" class="form-input w-50"></td>
                                    <?php
                                    $get_ip_add = getIPAddress();
                                    if (isset($_POST['update'])) {

                                        $cantitate = $_POST['cantitate'];
                                        $update = "update `cart` set cantitate=$cantitate where id_produs='$product_id'";
                                        $result1 = mysqli_query($con, $update);
                                        $total_price = $total_price * $cantitate;
                                    }
                                    ?>
                                    <td><?php echo $price_table ?>$</td>
                                    <td><input type="checkbox" name="removeitem[]" value="<?php echo $product_id ?>"></td>
                                    <td>
                                        <input type="submit" value="Update Produs" class="btn" name="update">
                                        <input type="submit" value="Sterge Produs" class="btn" name="remove">
                                    </td>
                                </tr>
                    <?php }
                        }
                    } else {

                        echo "<h2 class='text-center text-danger'>Cosul de cumparaturi este gol!</h2>";
                    }
                    ?>
                    </tbody>
                </table>

                <div class="d-flex">
                    <?php
                    $get_ip_add = getIPAddress();
                    $cart_query = "SELECT * FROM `cart` WHERE ip_address='$get_ip_add'";
                    $result = mysqli_query($con, $cart_query);
                    $result_count = mysqli_num_rows($result);
                    if ($result_count > 0) {
                        echo " <h4 class='px-3'>Subtotal:<strong style='color:#695cfe'>$total_price$</strong></h4>
                        <input type='submit' value='Continua Cumparaturile' class='btn' name='continua_cumparaturile'>
                        <input type='submit' value='Spre Plata'  style='margin-left: 15px' class='btn' name='spre_plata'>";
                    } else {
                        echo "<input type='submit' value='Continua Cumparaturile' class='btn' name='continua_cumparaturile'>";
                    }

                    if (isset($_POST['continua_cumparaturile'])) {
                        echo "<script>window.open('buchete.php', '_self')</script>";
                    }

                    if (isset($_POST['spre_plata'])) {
                        echo "<script>window.open('plata.php?id=$res_Id', '_self')</script>";
                    }
                    ?>

                </div>
        </div>


    </div>
    </form>

    <!--functie pt a sterge Produs-->

    <?php
    function remove_produs()
    {
        global $con;
        if (isset($_POST['remove'])) {
            foreach ($_POST['removeitem'] as $remove_id) {
                echo $remove_id;
                $delete_query = "DELETE  FROM `cart` WHERE id_produs=$remove_id";
                $run_delete = mysqli_query($con, $delete_query);

                if ($run_delete) {
                    echo "<script> window.open('cart.php', '_self')</script>";
                }
            }
        }
    }
    $remove_item = remove_produs();
    ?>

</body>

</html> ;