<?php
session_start();

include("php/config.php");
if (isset($_POST['adauga'])) {
    $nume = $_POST['nume'];
    $descriere = $_POST['descriere'];
    $pret = $_POST['pret'];
    $status = "true";

    //accesare poza
    $poza = $_FILES['poza']['name'];

    //accesare nume temporar poza
    $temp_poza = $_FILES['poza']['tmp_name'];

    //mutam poza in folderul buchete
    move_uploaded_file($temp_poza, "./buchete/$poza"); {
    }

    //query pt inserarea produsului
    $insert_buchet = "insert into `buchet` (nume, pret, descriere, poza, data, status)
    values ('$nume', '$pret', '$descriere', '$poza', NOW(), '$status')";

    $result = mysqli_query($con, $insert_buchet);

    if ($result) {
        echo "<script>alert('Produs adaugat cu succes!')</script>";
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style/style.css">
    <link href='https://unpkg.com/boxicons@2.1.1/css/boxicons.min.css' rel='stylesheet'>


    <title>Home</title>
</head>


<body style="background-image: url('background/5.jpg'); background-size: cover; background-repeat: no-repeat; background-attachment: fixed;">

    <div class="nav">
        <div class="logo">
            <p><a href="home.php"> Floraria ta online</a></p>
        </div>

        <div class="right-links">

            <?php
            $id = $_SESSION['id'];
            $query = mysqli_query($con, "SELECT*FROM users WHERE id=$id");

            while ($result = mysqli_fetch_assoc($query)) {
                $res_Uname = $result['username'];
                $res_Email = $result['email'];
                $res_Cont = $result['cont'];
                $res_Telefon = $result['telefon'];
                $res_Id = $result['id'];
            }

            echo "<a href='edit.php?id=$res_Id'>Change Profile</a>";
            ?>
            <a href="php/logout.php"><button class="btn">Log Out</button></a>
        </div>
    </div>


    <nav class="sidebar close">
        <header>
            <div class="image-text">
                <span class="image">
                    <img src="logo.jpg" alt="logo">
                </span>
                <div class="text header-text">
                    <span class="name">Pagina</span>
                    <span class="profession"><?php echo $res_Cont ?></span>
                </div>
            </div>
            <i class='bx bx-chevron-right toggle'></i>
        </header>
        <div class="menu-bar">
            <div class="menu">
                <ul class="menu-links">

                    <li class="nav-link">
                        <a href="home.php">
                            <i class='bx bx-home icon'></i>
                            <span class="text nav-text">HOME</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="VIEW_FLOWERS.php">
                            <i class='bx bxs-florist icon'></i>
                            <span class="text nav-text">Vizualizare Buchete</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="ADD.php">
                            <i class='bx bx-plus icon'></i>
                            <span class="text nav-text">Adauga Buchet</span>
                        </a>
                    </li>


                    <li class="nav-link">
                        <a href="ORDERS_ADMIN.php">
                            <i class='bx bx-shopping-bag icon'></i>
                            <span class="text nav-text">Comenzi</span>
                        </a>
                    </li>

                    <li class="nav-link">
                        <a href="VIEW_USER.php">
                            <i class='bx bx-user icon'></i>
                            <span class="text nav-text">Utilizatori</span>
                        </a>
                    </li>

                </ul>
            </div>

        </div>
    </nav>
    <script src="script.js"></script>


    <div class="container">
        <div class="box form-box">
            <form action="" method="post" enctype="multipart/form-data">
                <div class="field input">
                    <label for="nume">Nume</label>
                    <input type="text" name="nume" id="nume" placeholder="Completeaza Numele" autocomplete="off" required="required">
                </div>

                <div class="field input">
                    <label for="pret">Pret</label>
                    <input type="text" name="pret" id="pret" placeholder="Completeaza Pret" autocomplete="off" required="required">
                </div>

                <div class="field input">
                    <label for="descriere">Descriere</label>
                    <input type="text" name="descriere" id="descreiere" placeholder="Descrie Produsul" autocomplete="off" required>
                </div>

                <div class="field input">
                    <label for="poza">Poza</label>
                    <input type="file" name="poza" id="poza" required="required">
                </div>


                <div class="field">
                    <input type="submit" class="btn" name="adauga" value="Adauga" required>
                </div>
            </form>
        </div>
    </div>


</body>

</html>